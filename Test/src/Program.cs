﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Markwardt.Crypto;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
            => new Program().Start(args).Wait();

        public async Task Start(IEnumerable<string> arguments)
        {
            /*var protocol = new SecureProtocol(new RsaKeyGenerator(2048), new AesKeyGenerator(256));

            var peer1Data = protocol.GeneratePeer().Export();
            var peer2Data = protocol.GeneratePeer().Export();

            var peer1 = protocol.ImportPeer(peer1Data);
            var peer2 = protocol.ImportPeer(peer2Data);

            SessionKeyRequest request = peer1.RequestSessionKey();
            
            (ISymmetricKey peer2Key, byte[] response) = peer2.ReceiveSessionKey(request.Data);

            ISymmetricKey peer1Key = request.Accept(response);

            Console.WriteLine(await peer1Key.DecryptText(await peer2Key.EncryptText("hello bro")));
            Console.WriteLine(await peer2Key.DecryptText(await peer1Key.EncryptText("yo back bro")));*/

            var protocol = new CertificateProtocol(new RsaKeyGenerator(2048), new AesKeyGenerator(256));
            
            PrivateCertificate authority = protocol.GenerateCertificateRoot();
            
            CertificateRequest request1 = protocol.GenerateCertificateRequest(null, ("Name", "FIRST"));
            (Certificate certificate1Authority, byte[] response1) = authority.Issue(request1.Data);
            PrivateCertificate certificate1 = request1.Accept(response1);

            CertificateRequest request2 = protocol.GenerateCertificateRequest(null, ("Name", "SECOND"));
            (Certificate certificate2Authority, byte[] response2) = authority.Issue(request2.Data);
            PrivateCertificate certificate2 = request2.Accept(response2);

            PrivateCertificate fakeAuthority = protocol.GenerateCertificateRoot();

            CertificateRequest request3 = protocol.GenerateCertificateRequest();
            (Certificate certificate3Authority, byte[] response3) = fakeAuthority.Issue(request3.Data);
            PrivateCertificate certificate3 = request1.Accept(response3);

            Console.WriteLine(authority.Verify());
            Console.WriteLine(certificate1.Verify());
            Console.WriteLine(certificate2.Verify());
            Console.WriteLine(certificate1.HasSameRoot(certificate2));
            Console.WriteLine(certificate1.Verify(certificate2));
            Console.WriteLine(certificate1.Verify(fakeAuthority));
            Console.WriteLine(certificate1.Verify(certificate3));

            CertificateSessionRequest request = certificate1.RequestSession();
            IncomingCertificateSession incoming = certificate2.ReceiveSession(request.SenderOpening);
            OutgoingCertificateSession outgoing = await request.Receive(incoming.ReceiverOpening);
            (CertificateSession session1, byte[] receiverCompletion) = await incoming.Receive(outgoing.SenderCompletion);
            CertificateSession session2 = await outgoing.Receive(receiverCompletion);

            Console.WriteLine(await session2.Key.DecryptText(await session1.Key.EncryptText($"hey hows it going to {session1.Target.Data.Attributes["Name"]}")));
            Console.WriteLine(await session1.Key.DecryptText(await session2.Key.EncryptText($"pretty good to {session2.Target.Data.Attributes["Name"]}")));

            (LinkedStream link1, LinkedStream link2) = LinkedStream.CreatePair();
            
            CertificateStream stream1 = null;
            CertificateStream stream2 = null;

            TaskUtils.Fork(async () => stream1 = await certificate1.RequestStream(link1));
            TaskUtils.Fork(async () => stream2 = await certificate2.ReceiveStream(link2));

            while (stream1 == null || stream2 == null)
            {
                await Task.Delay(10);
            }

            Console.WriteLine(await stream1.Session.Key.DecryptText(await stream2.Session.Key.EncryptText("woooo")));

            await stream1.WriteTextAsync("some stuff");
            Console.WriteLine(await stream2.ReadTextAsync());
            await stream1.WriteTextAsync("some more stuff");
            Console.WriteLine(await stream2.ReadTextAsync());
            await stream1.WriteTextAsync("some extra stuff");
            Console.WriteLine(await stream2.ReadTextAsync());
        }
    }
}
