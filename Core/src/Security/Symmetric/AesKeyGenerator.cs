using System.Security.Cryptography;

namespace Markwardt.Crypto
{
    public class AesKeyGenerator : ISymmetricKeyGenerator
    {
        public static byte[] GenerateIv()
        {
            using (var aes = new AesManaged())
            {
                aes.GenerateIV();

                return aes.IV;
            }
        }

        public AesKeyGenerator(int bits)
        {
            this.bits = bits;
        }

        private readonly int bits;

        public ISymmetricKey Create()
        {
            using (var aes = new AesManaged())
            {
                aes.KeySize = bits;

                return new AesKey(aes.Key, aes.IV);
            }
        }

        public ISymmetricKey Generate()
        {
            using (var aes = new AesManaged())
            {
                aes.KeySize = bits;
                aes.GenerateKey();
                aes.GenerateIV();

                return new AesKey(aes.Key, aes.IV);
            }
        }
    }
}