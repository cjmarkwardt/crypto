namespace Markwardt.Crypto
{
    public interface ISymmetricKeyGenerator
    {
        ISymmetricKey Create();

        ISymmetricKey Generate();
    }

    public static class ISymmetricKeyGeneratorUtils
    {
        public static ISymmetricKey Import(this ISymmetricKeyGenerator generator, byte[] data)
        {
            ISymmetricKey key = generator.Create();
            key.Import(data);
            return key;
        }
    }
}