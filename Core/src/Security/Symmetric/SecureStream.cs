using System;
using System.IO;
using System.Threading.Tasks;

namespace Markwardt.Crypto
{
    public class SecureStream : Stream
    {
        public SecureStream(Stream stream, ISymmetricKey key)
        {
            this.stream = stream;
            this.key = key;
        }

        private readonly Stream stream;
        private readonly ISymmetricKey key;

        private byte[] buffer;
        private int bufferPosition;
        private int remainingBuffer => buffer == null ? 0 : buffer.Length - bufferPosition;

        public override bool CanRead => true;

        public override bool CanSeek => false;

        public override bool CanWrite => true;

        public override long Length => throw new System.NotImplementedException();

        public override long Position { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public override void Flush()
            => stream.Flush();

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new System.NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new System.NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new System.NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new System.NotImplementedException();
        }

        public override async Task<int> ReadAsync(byte[] buffer, int offset, int count, System.Threading.CancellationToken cancellationToken)
        {
            int remaining = count;
            var data = new MemoryStream(buffer, offset, count);

            if (buffer == null)
            {
                await ReadIntoBuffer();
            }

            while (remaining > remainingBuffer)
            {
                remaining -= remainingBuffer;
                data.Write(CopyFromBuffer(remainingBuffer));
                await ReadIntoBuffer();
            }

            if (remaining > 0)
            {
                data.Write(CopyFromBuffer(remaining));
            }

            return count;
        }

        public override async Task WriteAsync(byte[] buffer, int offset, int count, System.Threading.CancellationToken cancellationToken)
        {
            await stream.WriteBlockAsync(await key.Encrypt(buffer.CopySubArray(offset, count)));
        }

        private async Task ReadIntoBuffer()
        {
            buffer = await key.Decrypt(await stream.ReadBlockAsync());
            bufferPosition = 0;
        }

        private byte[] CopyFromBuffer(int count)
        {
            byte[] data = buffer.CopySubArray(bufferPosition, remainingBuffer);
            bufferPosition += data.Length;
            return data;
        }
    }
}