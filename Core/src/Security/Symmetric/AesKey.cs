using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Markwardt.Crypto
{
    public class AesKey : ISymmetricKey
    {
        public AesKey(byte[] key, byte[] iv)
        {
            this.key = key;
            this.iv = iv;
        }

        private byte[] key;
        private byte[] iv;

        public async Task Decrypt(Stream input, Stream output)
        {
            using (var aes = new AesManaged())
            {
                using (var crypto = new CryptoStream(output, aes.CreateDecryptor(key, iv), CryptoStreamMode.Write))
                {
                    await input.CopyToAsync(crypto);
                }
            }
        }

        public async Task Encrypt(Stream input, Stream output)
        {
            using (var aes = new AesManaged())
            {
                using (var crypto = new CryptoStream(output, aes.CreateEncryptor(key, iv), CryptoStreamMode.Write))
                {
                    await input.CopyToAsync(crypto);
                }
            }
        }

        public byte[] Export()
        {
            var stream = new MemoryStream();
            stream.WriteBlock(key);
            stream.WriteBlock(iv);
            return stream.ToArray();
        }

        public void Import(byte[] keyData)
        {
            var stream = new MemoryStream(keyData);
            key = stream.ReadBlock();
            iv = stream.ReadBlock();
        }
    }
}