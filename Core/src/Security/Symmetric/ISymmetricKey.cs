using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Markwardt.Crypto
{
    public interface ISymmetricKey : IExportable
    {
        Task Encrypt(Stream input, Stream output);
        Task Decrypt(Stream input, Stream output);
    }

    public static class ISymmetricKeyUtils
    {
        public static async Task<byte[]> Encrypt(this ISymmetricKey key, byte[] input)
        {
            var inputStream = new MemoryStream(input);
            var outputStream = new MemoryStream();

            await key.Encrypt(inputStream, outputStream);

            return outputStream.ToArray();
        }

        public static async Task<byte[]> Decrypt(this ISymmetricKey key, byte[] input)
        {
            var inputStream = new MemoryStream(input);
            var outputStream = new MemoryStream();

            await key.Decrypt(inputStream, outputStream);

            return outputStream.ToArray();
        }

        public static async Task<byte[]> EncryptText(this ISymmetricKey key, string text)
            => await key.Encrypt(Encoding.Unicode.GetBytes(text));

        public static async Task<string> DecryptText(this ISymmetricKey key, byte[] encrypted)
            => Encoding.Unicode.GetString(await key.Decrypt(encrypted));
    }
}