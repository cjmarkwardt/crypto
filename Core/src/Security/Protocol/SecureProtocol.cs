using System.IO;

namespace Markwardt.Crypto
{
    public class SecureProtocol
    {
        public SecureProtocol(IAsymmetricKeyGenerator peerKeyGenerator, ISymmetricKeyGenerator sessionKeyGenerator)
        {
            PeerKeyGenerator = peerKeyGenerator;
            SessionKeyGenerator = sessionKeyGenerator;
        }

        public IAsymmetricKeyGenerator PeerKeyGenerator { get; private set; }
        public ISymmetricKeyGenerator SessionKeyGenerator { get; private set; }

        public SecurePeer ImportPeer(IPublicKey publicKey, IPrivateKey privateKey)
            => new SecurePeer(this, publicKey, privateKey);

        public SecurePeer ImportPeer(byte[] data)
        {
            var stream = new MemoryStream(data);
            IPublicKey publicKey = PeerKeyGenerator.ImportPublic(stream.ReadBlock());
            IPrivateKey privateKey = PeerKeyGenerator.ImportPrivate(stream.ReadBlock());
            return ImportPeer(publicKey, privateKey);
        }

        public SecurePeer GeneratePeer()
        {
            (IPublicKey publicKey, IPrivateKey privateKey) = PeerKeyGenerator.Generate();
            return ImportPeer(publicKey, privateKey);
        }
    }
}