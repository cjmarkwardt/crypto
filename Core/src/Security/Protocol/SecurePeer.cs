using System.IO;
using System.Threading.Tasks;

namespace Markwardt.Crypto
{
    public class SecurePeer
    {
        public SecurePeer(SecureProtocol protocol, IPublicKey publicKey, IPrivateKey privateKey)
        {
            Protocol = protocol;
            PublicKey = publicKey;
            PrivateKey = privateKey;
        }

        public SecureProtocol Protocol { get; private set; }
        public IPublicKey PublicKey { get; private set; }
        public IPrivateKey PrivateKey { get; private set; }

        public async Task<SecureStream> RequestStream(Stream stream)
        {
            SessionKeyRequest request = RequestSessionKey();
            await stream.WriteBlockAsync(request.Data);
            ISymmetricKey key = request.Accept(await stream.ReadBlockAsync());
            return new SecureStream(stream, key);
        }

        public async Task<SecureStream> ReceiveStream(Stream stream)
        {
            (ISymmetricKey key, byte[] response) = ReceiveSessionKey(await stream.ReadBlockAsync());
            await stream.WriteBlockAsync(response);
            return new SecureStream(stream, key);
        }

        public SessionKeyRequest RequestSessionKey()
            => new SessionKeyRequest(this);

        public (ISymmetricKey key, byte[] response) ReceiveSessionKey(byte[] request)
        {
            IPublicKey target = Protocol.PeerKeyGenerator.CreatePublic();
            target.Import(request);

            ISymmetricKey key = Protocol.SessionKeyGenerator.Generate();
            byte[] encryptedKey = target.Encrypt(key.Export());

            return (key, encryptedKey);
        }

        public byte[] Export()
        {
            var stream = new MemoryStream();
            stream.WriteBlock(PublicKey.Export());
            stream.WriteBlock(PrivateKey.Export());
            return stream.ToArray();
        }
    }
}