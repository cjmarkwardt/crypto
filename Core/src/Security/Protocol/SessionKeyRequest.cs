using System.IO;

namespace Markwardt.Crypto
{
    public class SessionKeyRequest
    {
        public SessionKeyRequest(SecurePeer peer)
        {
            this.peer = peer;
            Data = peer.PublicKey.Export();
        }
        
        private readonly SecurePeer peer;

        public byte[] Data { get; private set; }

        public ISymmetricKey Accept(byte[] response)
        {
            byte[] keyData = peer.PrivateKey.Decrypt(response);
            return peer.Protocol.SessionKeyGenerator.Import(keyData);
        }
    }
}