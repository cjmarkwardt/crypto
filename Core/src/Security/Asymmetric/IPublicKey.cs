using System.IO;
using System.Text;

namespace Markwardt.Crypto
{
    public interface IPublicKey : IExportable
    {
        byte[] Encrypt(byte[] data);
        bool Verify(Stream data, byte[] signature);
    }

    public static class IPublicKeyUtils
    {
        public static bool Verify(this IPublicKey key, byte[] data, byte[] signature)
            => key.Verify(new MemoryStream(data), signature);

        public static bool Verify(this IPublicKey key, string text, byte[] signature)
            => key.Verify(Encoding.Unicode.GetBytes(text), signature);

        public static (ISymmetricKey key, byte[] encryptedKey) CreateSessionKey(this IPublicKey key, ISymmetricKeyGenerator generator)
        {
            ISymmetricKey sessionKey = generator.Generate();
            return (sessionKey, key.Encrypt(sessionKey.Export()));
        }
    }
}