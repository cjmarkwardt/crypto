using System.IO;
using System.Text;

namespace Markwardt.Crypto
{
    public interface IPrivateKey : IExportable
    {
        byte[] Decrypt(byte[] encrypted);
        byte[] Sign(Stream data);
    }

    public static class IPrivateKeyUtils
    {
        public static byte[] Sign(this IPrivateKey key, byte[] data)
            => key.Sign(new MemoryStream(data));

        public static byte[] Sign(this IPrivateKey key, string text)
            => key.Sign(Encoding.Unicode.GetBytes(text));

        public static ISymmetricKey ReadSessionKey(this IPrivateKey key, byte[] encrypted, ISymmetricKeyGenerator generator)
        {
            ISymmetricKey sessionKey = generator.Create();
            byte[] keyData = key.Decrypt(encrypted);
            sessionKey.Import(keyData);
            return sessionKey;;
        }
    }
}