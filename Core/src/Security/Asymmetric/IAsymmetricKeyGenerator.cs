namespace Markwardt.Crypto
{
    public interface IAsymmetricKeyGenerator
    {
        IPublicKey CreatePublic();
        IPrivateKey CreatePrivate();
        
        (IPublicKey, IPrivateKey) Generate();
    }

    public static class IAsymmetricKeyGeneratorUtils
    {
        public static IPublicKey ImportPublic(this IAsymmetricKeyGenerator generator, byte[] data)
        {
            IPublicKey key = generator.CreatePublic();
            key.Import(data);
            return key;
        }

        public static IPrivateKey ImportPrivate(this IAsymmetricKeyGenerator generator, byte[] data)
        {
            IPrivateKey key = generator.CreatePrivate();
            key.Import(data);
            return key;
        }
    }
}