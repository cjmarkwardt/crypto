using System.Security.Cryptography;

namespace Markwardt.Crypto
{
    public class RsaKeyGenerator : IAsymmetricKeyGenerator
    {
        public RsaKeyGenerator(int bits)
        {
            this.bits = bits;
        }

        private readonly int bits;

        public IPublicKey CreatePublic()
        {
            (IPublicKey publicKey, IPrivateKey privateKey) = Generate();
            return publicKey;
        }

        public IPrivateKey CreatePrivate()
        {
            (IPublicKey publicKey, IPrivateKey privateKey) = Generate();
            return privateKey;
        }

        public (IPublicKey, IPrivateKey) Generate()
        {
            using (var rsa = new RSACryptoServiceProvider(bits))
            {
                return
                (
                    new RsaKey(rsa.ExportParameters(false), false),
                    new RsaKey(rsa.ExportParameters(true), true)
                );
            }
        }
    }
}