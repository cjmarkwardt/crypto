using System.IO;
using System.Security.Cryptography;

namespace Markwardt.Crypto
{
    public class RsaKey : IPublicKey, IPrivateKey
    {
        public RsaKey(RSAParameters parameters, bool isPrivate)
        {
            this.parameters = parameters;
            this.isPrivate = isPrivate;
        }

        private RSAParameters parameters;
        private bool isPrivate;

        public byte[] Export()
        {
            var stream = new MemoryStream();

            stream.WriteBlock(parameters.Exponent);
            stream.WriteBlock(parameters.Modulus);

            if (isPrivate)
            {
                stream.WriteByte(1);
                stream.WriteBlock(parameters.D);
                stream.WriteBlock(parameters.DP);
                stream.WriteBlock(parameters.DQ);
                stream.WriteBlock(parameters.InverseQ);
                stream.WriteBlock(parameters.P);
                stream.WriteBlock(parameters.Q);
            }
            else
            {
                stream.WriteByte(0);
            }

            return stream.ToArray();
        }

        public void Import(byte[] keyData)
        {
            var stream = new MemoryStream(keyData);
            parameters = new RSAParameters();

            parameters.Exponent = stream.ReadBlock();
            parameters.Modulus = stream.ReadBlock();

            isPrivate = stream.ReadByte() == 1;
            if (isPrivate)
            {
                parameters.D = stream.ReadBlock();
                parameters.DP = stream.ReadBlock();
                parameters.DQ = stream.ReadBlock();
                parameters.InverseQ = stream.ReadBlock();
                parameters.P = stream.ReadBlock();
                parameters.Q = stream.ReadBlock();
            }
        }

        public byte[] Decrypt(byte[] encrypted)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(parameters);
                return rsa.Decrypt(encrypted, RSAEncryptionPadding.Pkcs1);
            }
        }

        public byte[] Encrypt(byte[] data)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(parameters);
                return rsa.Encrypt(data, RSAEncryptionPadding.Pkcs1);
            }
        }

        public byte[] Sign(Stream data)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(parameters);
                return rsa.SignData(data, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            }
        }

        public bool Verify(Stream data, byte[] signature)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(parameters);
                return rsa.VerifyData(data, signature, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            }
        }
    }
}