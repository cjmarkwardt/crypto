using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Markwardt.Crypto
{
    public class IncomingCertificateSession
    {
        public IncomingCertificateSession(PrivateCertificate certificate, byte[] request)
        {
            this.certificate = certificate;

            target = new Certificate(certificate.Protocol, request);
            
            if (!target.Verify())
            {
                throw new ArgumentException();
            }

            if (certificate.Protocol.EnforceSameRoot && !certificate.HasSameRoot(target))
            {
                throw new ArgumentException();
            }

            senderTest = RandomUtils.GenerateSecureBytes(certificate.Protocol.TestLength);

            ReceiverOpening = ByteUtils.WriteBlocks
            (
                certificate.Export(),
                target.Data.PublicKey.Encrypt(senderTest)
            );
        }

        private readonly PrivateCertificate certificate;
        private readonly Certificate target;
        private readonly byte[] senderTest;

        public byte[] ReceiverOpening { get; private set; }

        public async Task<(CertificateSession session, byte[] receiverCompletion)> Receive(byte[] senderCompletion)
        {
            IList<byte[]> blocks = senderCompletion.ReadBlocks(3);

            ISymmetricKey key = certificate.Protocol.SessionKeyGenerator.Import(certificate.PrivateKey.Decrypt(blocks[0]));
            byte[] receiverProof = certificate.PrivateKey.Decrypt(blocks[1]);
            byte[] senderProof = await key.Decrypt(blocks[2]);

            if (!senderTest.SequenceEqual(senderProof))
            {
                throw new ArgumentException();
            }

            var session = new CertificateSession(certificate, target, key);

            return (session, await key.Encrypt(receiverProof));
        }
    }
}