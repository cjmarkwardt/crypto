using System.Collections.Generic;
using System.Threading.Tasks;

namespace Markwardt.Crypto
{
    public class CertificateSessionRequest
    {
        public CertificateSessionRequest(PrivateCertificate certificate)
        {
            this.certificate = certificate;

            SenderOpening = certificate.Export();
        }

        private readonly PrivateCertificate certificate;

        public byte[] SenderOpening { get; private set; }

        public async Task<OutgoingCertificateSession> Receive(byte[] receiverOpening)
        {
            IList<byte[]> blocks = receiverOpening.ReadBlocks(2);

            Certificate target = new Certificate(certificate.Protocol, blocks[0]);
            byte[] senderProof = certificate.PrivateKey.Decrypt(blocks[1]);

            return await OutgoingCertificateSession.Create(certificate, target, senderProof);
        }
    }
}