using System;
using System.Linq;
using System.Threading.Tasks;

namespace Markwardt.Crypto
{
    public class OutgoingCertificateSession
    {
        public static async Task<OutgoingCertificateSession> Create(PrivateCertificate certificate, Certificate target, byte[] senderProof)
        {
            var outgoing = new OutgoingCertificateSession();
            
            outgoing.certificate = certificate;
            outgoing.target = target;

            if (!target.Verify())
            {
                throw new ArgumentException();
            }

            if (certificate.Protocol.EnforceSameRoot && !certificate.HasSameRoot(target))
            {
                throw new ArgumentException();
            }

            outgoing.receiverTest = RandomUtils.GenerateSecureBytes(certificate.Protocol.TestLength);
            outgoing.key = certificate.Protocol.SessionKeyGenerator.Generate();
            
            outgoing.SenderCompletion = ByteUtils.WriteBlocks
            (
                target.Data.PublicKey.Encrypt(outgoing.key.Export()),
                target.Data.PublicKey.Encrypt(outgoing.receiverTest),
                await outgoing.key.Encrypt(senderProof)
            );

            return outgoing;
        }

        private OutgoingCertificateSession() { }

        private PrivateCertificate certificate;
        private Certificate target;
        private byte[] receiverTest;
        private ISymmetricKey key;

        public byte[] SenderCompletion { get; private set; }

        public async Task<CertificateSession> Receive(byte[] receiverCompletion)
        {
            byte[] receiverProof = await key.Decrypt(receiverCompletion);

            if (!receiverTest.SequenceEqual(receiverProof))
            {
                throw new ArgumentException();
            }

            return new CertificateSession(certificate, target, key);
        }
    }
}