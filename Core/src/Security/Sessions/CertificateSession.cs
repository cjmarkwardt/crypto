namespace Markwardt.Crypto
{
    public class CertificateSession
    {
        public CertificateSession(PrivateCertificate certificate, Certificate target, ISymmetricKey key)
        {
            Certificate = certificate;
            Target = target;
            Key = key;
        }

        public PrivateCertificate Certificate { get; private set; }
        public Certificate Target { get; private set; }
        public ISymmetricKey Key { get; private set; }
    }
}