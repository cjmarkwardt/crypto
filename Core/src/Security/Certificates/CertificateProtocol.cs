using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Markwardt.Crypto
{
    public class CertificateProtocol : SecureProtocol
    {
        public CertificateProtocol(IAsymmetricKeyGenerator peerKeyGenerator, ISymmetricKeyGenerator sessionKeyGenerator, bool enforceSameRoot = true, int testLength = 32, ICertificateRequestValidator requestValidator = null)
            : base(peerKeyGenerator, sessionKeyGenerator)
        {
            EnforceSameRoot = enforceSameRoot;
            TestLength = testLength;
            RequestValidator = requestValidator ?? new OpenRequestValidator();
        }

        public bool EnforceSameRoot { get; private set; }
        public int TestLength { get; private set; }
        public ICertificateRequestValidator RequestValidator { get; private set; }

        public Certificate ImportCertificate(byte[] data)
            => new Certificate(this, data);

        public PrivateCertificate ImportPrivateCertificate(byte[] data)
            => new PrivateCertificate(this, data);

        public PrivateCertificate GenerateCertificateRoot(DateTime? expirationDate = null, IDictionary<string, string> attributes = null)
        {
            (IPublicKey publicKey, IPrivateKey privateKey) = PeerKeyGenerator.Generate();
            return PrivateCertificate.CreateRoot(this, publicKey, privateKey, expirationDate, attributes);
        }

        public CertificateRequest GenerateCertificateRequest(DateTime? expirationDate = null, IDictionary<string, string> attributes = null)
        {
            (IPublicKey publicKey, IPrivateKey privateKey) = PeerKeyGenerator.Generate();
            return new CertificateRequest(this, new CertificateRequestData(this, publicKey, expirationDate, attributes), privateKey);
        }

        public CertificateRequest GenerateCertificateRequest(DateTime? expirationDate = null, params (string key, string value)[] attributes)
            => GenerateCertificateRequest(expirationDate, attributes.ToDictionary(attribute => attribute.key, attribute => attribute.value));
    }
}