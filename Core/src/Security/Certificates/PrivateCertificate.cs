using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Markwardt.Crypto
{
    public class PrivateCertificate : Certificate
    {
        public static PrivateCertificate CreateRoot(CertificateProtocol protocol, IPublicKey publicKey, IPrivateKey privateKey, DateTime? expirationDate = null, IDictionary<string, string> attributes = null)
        {
            var data = new CertificateData(protocol, publicKey, null, DateTime.Now, expirationDate, attributes);
            return new PrivateCertificate()
            {
                Protocol = protocol,
                Data = data,
                PrivateKey = privateKey,
                Signature = privateKey.Sign(data.Export()),
            };
        }

        public PrivateCertificate(CertificateProtocol protocol, byte[] data)
        {
            Protocol = protocol;
            ImportPrivate(data);
        }

        public PrivateCertificate(CertificateProtocol protocol, CertificateData data, byte[] signature, IPrivateKey privateKey)
            : base(protocol, data, signature)
        {
            PrivateKey = privateKey;
        }

        private PrivateCertificate() { }

        public IPrivateKey PrivateKey { get; private set; }

        public async Task<CertificateStream> RequestStream(Stream stream)
        {
            CertificateSessionRequest request = RequestSession();
            await stream.WriteBlockAsync(request.SenderOpening);
            OutgoingCertificateSession outgoing = await request.Receive(await stream.ReadBlockAsync());
            await stream.WriteBlockAsync(outgoing.SenderCompletion);
            CertificateSession session = await outgoing.Receive(await stream.ReadBlockAsync());
            return new CertificateStream(stream, session);
        }

        public async Task<CertificateStream> ReceiveStream(Stream stream)
        {
            IncomingCertificateSession incoming = ReceiveSession(await stream.ReadBlockAsync());
            await stream.WriteBlockAsync(incoming.ReceiverOpening);
            (CertificateSession session, byte[] receiverCompletion) = await incoming.Receive(await stream.ReadBlockAsync());
            await stream.WriteBlockAsync(receiverCompletion);
            return new CertificateStream(stream, session);
        }

        public CertificateSessionRequest RequestSession()
            => new CertificateSessionRequest(this);

        public IncomingCertificateSession ReceiveSession(byte[] senderOpening)
            => new IncomingCertificateSession(this, senderOpening);

        public byte[] ExportPrivate()
        {
            var stream = new MemoryStream();
            stream.WriteBlock(base.Export());
            stream.WriteBlock(PrivateKey.Export());
            return stream.ToArray();
        }

        public void ImportPrivate(byte[] data)
        {
            var stream = new MemoryStream(data);
            Import(stream.ReadBlock());
            PrivateKey = Protocol.PeerKeyGenerator.ImportPrivate(stream.ReadBlock());
        }

        public (Certificate certificate, byte[] response) Issue(byte[] request)
        {
            var requestData = new CertificateRequestData(Protocol, request);
            DateTime issueDate = DateTime.Now;

            if ((requestData.ExpirationDate != null && requestData.ExpirationDate.Value < issueDate) || !Protocol.RequestValidator.IsValid(requestData))
            {
                return (null, new byte[0]);
            }

            CertificateData data = requestData.CreateIssuedData(this, issueDate);
            byte[] signature = PrivateKey.Sign(data.Export());

            var certificate = new Certificate(Protocol, data, signature);

            var stream = new MemoryStream();
            stream.WriteBlock(data.Export());
            stream.WriteBlock(signature);
            byte[] response = stream.ToArray();

            return (certificate, response);
        }
    }
}