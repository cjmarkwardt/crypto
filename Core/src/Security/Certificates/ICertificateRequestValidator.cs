using System;
using System.Collections.Generic;

namespace Markwardt.Crypto
{
    public interface ICertificateRequestValidator
    {
        bool IsValid(CertificateRequestData data);
    }
}