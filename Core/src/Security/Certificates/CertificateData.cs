using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Markwardt.Crypto
{
    public class CertificateData : CertificateRequestData
    {
        public CertificateData(CertificateProtocol protocol, byte[] data)
        {
            Protocol = protocol;
            Import(data);
        }

        public CertificateData(CertificateProtocol protocol, IPublicKey publicKey, Certificate issuer, DateTime issueDate, DateTime? expirationDate, IDictionary<string, string> attributes)
            : base(protocol, publicKey, expirationDate, attributes)
        {
            Issuer = issuer;
            IssueDate = issueDate;
        }

        public Certificate Issuer { get; private set; }
        public DateTime IssueDate { get; private set; }

        public override byte[] Export()
        {
            var stream = new MemoryStream();

            if (Issuer != null)
            {
                stream.WriteByte(1);
                stream.WriteBlock(Issuer.Export());
            }
            else
            {
                stream.WriteByte(0);
            }
            
            stream.Write(BitConverter.GetBytes(IssueDate.Ticks));
            stream.WriteBlock(base.Export());
            return stream.ToArray();
        }

        public override void Import(byte[] data)
        {
            var stream = new MemoryStream(data);

            if (stream.ReadByte() == 1)
            {
                Issuer = new Certificate(Protocol, stream.ReadBlock());
            }

            IssueDate = new DateTime(BitConverter.ToInt64(stream.Read(8), 0));
            base.Import(stream.ReadBlock());
        }
    }
}