using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Markwardt.Crypto
{
    public class Certificate : IExportable, IEquatable<Certificate>
    {
        public Certificate(CertificateProtocol protocol, byte[] data)
        {
            Protocol = protocol;
            Import(data);
        }

        public Certificate(CertificateProtocol protocol, CertificateData data, byte[] signature)
        {
            Protocol = protocol;
            Data = data;
            Signature = signature;
        }

        protected Certificate() { }

        public CertificateProtocol Protocol { get; protected set; }
        public CertificateData Data { get; protected set; }
        public byte[] Signature { get; protected set; }

        public bool IsRoot => Data.Issuer == null;
        public Certificate Root => IsRoot ? this : Data.Issuer.Root;

        public bool Verify()
        {
            Certificate current = this;
            while (current.Data.Issuer != null)
            {
               if (!current.Data.Issuer.Data.PublicKey.Verify(current.Data.Export(), current.Signature))
               {
                   return false;
               }

               current = current.Data.Issuer;
            }

            return current.Data.PublicKey.Verify(current.Data.Export(), current.Signature);
        }

        public bool Verify(Certificate other)
            => HasSameRoot(other) && Verify() && other.Verify();

        public bool HasSameRoot(Certificate other)
            => Root.Equals(other.Root);

        public byte[] Export()
        {
            var stream = new MemoryStream();
            stream.WriteBlock(Data.Export());
            stream.WriteBlock(Signature);
            return stream.ToArray();
        }

        public void Import(byte[] data)
        {
            var stream = new MemoryStream(data);
            Data = new CertificateData(Protocol, stream.ReadBlock());
            Signature = stream.ReadBlock();
        }

        public bool Equals(Certificate other)
        {
            return Data.Export().SequenceEqual(other.Data.Export()) && Signature.SequenceEqual(other.Signature);
        }
    }
}