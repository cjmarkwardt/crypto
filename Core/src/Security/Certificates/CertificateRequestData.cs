using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Markwardt.Crypto
{
    public class CertificateRequestData : IExportable
    {
        public CertificateRequestData(CertificateProtocol protocol, byte[] data)
        {
            Protocol = protocol;
            Import(data);
        }

        public CertificateRequestData(CertificateProtocol protocol, IPublicKey publicKey, DateTime? expirationDate, IDictionary<string, string> attributes)
        {
            Protocol = protocol;
            PublicKey = publicKey;
            ExpirationDate = expirationDate;
            Attributes = attributes == null ? new Dictionary<string, string>() : attributes.ToDictionary(attribute => attribute.Key, attribute => attribute.Value);
        }

        protected CertificateRequestData() { }

        public CertificateProtocol Protocol { get; protected set; }

        public IPublicKey PublicKey { get; private set; }
        public DateTime? ExpirationDate { get; private set; }
        public IReadOnlyDictionary<string, string> Attributes { get; private set; }

        public CertificateData CreateIssuedData(Certificate issuer, DateTime issueDate)
            => new CertificateData(Protocol, PublicKey, issuer, issueDate, ExpirationDate, Attributes.ToDictionary(attribute => attribute.Key, attribute => attribute.Value));

        public virtual byte[] Export()
        {
            var stream = new MemoryStream();
            stream.WriteBlock(PublicKey.Export());
            
            if (ExpirationDate != null)
            {
                stream.WriteByte(1);
                stream.Write(BitConverter.GetBytes(ExpirationDate.Value.Ticks));
            }
            else
            {
                stream.WriteByte(0);
            }

            stream.Write(BitConverter.GetBytes(Attributes.Count));
            foreach (KeyValuePair<string, string> attribute in Attributes)
            {
                stream.WriteBlock(Encoding.Unicode.GetBytes(attribute.Key));
                stream.WriteBlock(Encoding.Unicode.GetBytes(attribute.Value));
            }

            return stream.ToArray();
        }

        public virtual void Import(byte[] keyData)
        {
            var stream = new MemoryStream(keyData);

            PublicKey = Protocol.PeerKeyGenerator.ImportPublic(stream.ReadBlock());

            ExpirationDate = null;
            if (stream.ReadByte() == 1)
            {
                ExpirationDate = new DateTime(BitConverter.ToInt64(stream.Read(8), 0));
            }

            var attributes = new Dictionary<string, string>();
            int attributeCount = BitConverter.ToInt32(stream.Read(4), 0);
            for (int i = 0; i < attributeCount; i++)
            {
                attributes.Add(Encoding.Unicode.GetString(stream.ReadBlock()), Encoding.Unicode.GetString(stream.ReadBlock()));
            }

            Attributes = attributes;
        }
    }
}