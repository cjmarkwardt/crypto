using System.IO;

namespace Markwardt.Crypto
{
    public class CertificateStream : SecureStream
    {
        public CertificateStream(Stream stream, CertificateSession session)
            : base(stream, session.Key)
        {
            Session = session;
        }

        public CertificateSession Session { get; private set; }
    }
}