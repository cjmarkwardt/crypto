using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Markwardt.Crypto
{
    public class CertificateRequest
    {
        public CertificateRequest(CertificateProtocol protocol, CertificateRequestData data, IPrivateKey privateKey)
        {
            this.protocol = protocol;
            this.data = data;
            this.privateKey = privateKey;

            Data = data.Export();
        }

        private readonly CertificateProtocol protocol;
        private readonly CertificateRequestData data;
        private readonly IPrivateKey privateKey;

        public byte[] Data { get; private set; }

        public PrivateCertificate Accept(byte[] response)
        {
            var stream = new MemoryStream(response);
            var data = new CertificateData(protocol, stream.ReadBlock());
            byte[] signature = stream.ReadBlock();

            return new PrivateCertificate(protocol, data, signature, privateKey);
        }
    }
}