using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Markwardt.Crypto
{
    public class LinkedStream : Stream
    {
        public static (LinkedStream, LinkedStream) CreatePair()
        {
            var stream1 = new LinkedStream();
            var stream2 = new LinkedStream();

            stream1.other = stream2;
            stream2.other = stream1;

            return (stream1, stream2);
        }

        private LinkedStream() { }

        private readonly MemoryStream data = new MemoryStream();

        private LinkedStream other;
        private long readPosition = 0;

        public override bool CanRead => true;

        public override bool CanSeek => false;

        public override bool CanWrite => true;

        public override long Length => throw new System.NotImplementedException();

        public override long Position { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public override void Flush() { }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new System.NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new System.NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new System.NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new System.NotImplementedException();
        }

        public override async Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            await data.WriteAsync(buffer, offset, count);
        }

        public override async Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            while (true)
            {
                long writePosition = other.data.Position;
                other.data.Position = readPosition;
                int read = other.data.Read(buffer, offset, count);
                readPosition = other.data.Position;
                other.data.Position = writePosition;

                if (read > 0)
                {
                    return read;
                }
                else
                {
                    await Task.Delay(50);
                }
            }
        }
    }
}