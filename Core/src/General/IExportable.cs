namespace Markwardt.Crypto
{
    public interface IExportable
    {
        byte[] Export();
        void Import(byte[] keyData);
    }
}