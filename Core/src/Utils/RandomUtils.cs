using System.Security.Cryptography;

namespace Markwardt.Crypto
{
    public static class RandomUtils
    {
        public static byte[] GenerateSecureBytes(int count)
        {
            using (var random = new RNGCryptoServiceProvider())
            {
                byte[] data = new byte[count];
                random.GetBytes(data);
                return data;
            }
        }
    }
}