using System;
using System.Collections.Generic;
using System.IO;

namespace Markwardt.Crypto
{
    public static class ByteUtils
    {
        public static byte[] WriteBlocks(params byte[][] blocks)
        {
            var stream = new MemoryStream();
            foreach (byte[] block in blocks)
            {
                stream.WriteBlock(block);
            }

            return stream.ToArray();
        }

        public static IList<byte[]> ReadBlocks(this byte[] data, int count)
        {
            var blocks = new List<byte[]>();
            var stream = new MemoryStream(data);
            for (int i = 0; i < count; i++)
            {
                blocks.Add(stream.ReadBlock());
            }

            return blocks;
        }

        public static byte[] CopySubArray(this byte[] data, int offset, int count)
        {
            byte[] subData = new byte[count];
            for (int i = 0; i < count; i++)
            {
                subData[i] = data[offset + i];
            }

            return subData;
        }
    }
}