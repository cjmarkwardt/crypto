using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Markwardt.Crypto
{
    public static class StreamUtils
    {
        public static void Write(this Stream stream, byte[] data)
            => stream.Write(data, 0, data.Length);

        public static byte[] Read(this Stream stream, int count)
        {
            byte[] data = new byte[count];
            int received = stream.Read(data, 0, count);
            if (received == 0)
            {
                return null;
            }
            else
            {
                Array.Resize(ref data, received);
                return data;
            }
        }

        public static async Task WriteAsync(this Stream stream, byte[] data)
            => await stream.WriteAsync(data, 0, data.Length);

        public static async Task<byte[]> ReadAsync(this Stream stream, int count)
        {
            byte[] data = new byte[count];
            int position = 0;
            while (position < count)
            {
                int received = await stream.ReadAsync(data, position, count - position);
                if (received == 0)
                {
                    throw new IOException($"Stream ended.");
                }
                else
                {
                    position += received;
                }
            }

            return data;
        }

        public static void WriteBlock(this Stream stream, byte[] block)
        {
            stream.Write(BitConverter.GetBytes(block.Length));
            stream.Write(block);
        }

        public static byte[] ReadBlock(this Stream stream)
        {
            int length = BitConverter.ToInt32(stream.Read(4), 0);
            return stream.Read(length);
        }

        public static async Task WriteBlockAsync(this Stream stream, byte[] block)
        {
            await stream.WriteAsync(BitConverter.GetBytes(block.Length));
            await stream.WriteAsync(block);
        }

        public static async Task<byte[]> ReadBlockAsync(this Stream stream)
        {
            int length = BitConverter.ToInt32(await stream.ReadAsync(4), 0);
            return await stream.ReadAsync(length);
        }

        public static void WriteText(this Stream stream, string text)
        {
            stream.WriteBlock(Encoding.Unicode.GetBytes(text));
        }

        public static string ReadText(this Stream stream)
        {
            return Encoding.Unicode.GetString(stream.ReadBlock());
        }

        public static async Task WriteTextAsync(this Stream stream, string text)
        {
            await stream.WriteBlockAsync(Encoding.Unicode.GetBytes(text));
        }

        public static async Task<string> ReadTextAsync(this Stream stream)
        {
            return Encoding.Unicode.GetString(await stream.ReadBlockAsync());
        }
    }
}