using System;
using System.Threading.Tasks;

namespace Markwardt.Crypto
{
    public static class TaskUtils
    {
        public static async void Fork(Func<Task> action)
            => await action();
    }
}